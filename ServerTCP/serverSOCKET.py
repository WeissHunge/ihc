import socket
import sys
import logging
import pyautogui

MAX_CONEXOES = 1
BUFFER_SIZE = 100
MOVEMENT_SPEED = 70
MOVEMENT_SPEED_TOUCH = 20 
pyautogui.FAILSAFE = False

class PedidoDeSaida(Exception):
    pass

def controlaGUI(comando):
	comando = str(comando)
	comando = comando.replace("$","")
	parte = comando.split(' ')
	azimuth,pitch,roll,left_click,right_click,uso_teclado,tecla,sair = parte
	sair = True if int(sair) == 1 else False
	if sair:
		raise PedidoDeSaida
	try:
		azimuth = float(azimuth)
		pitch = float(pitch)
		roll = float(roll)
		#print("pitch {} roll {}".format(pitch,roll))
		left_click = True if int(left_click) == 1 else False
		right_click = True if int(right_click) == 1 else False
		touchpad = True if int(uso_teclado) == 2 else False
		uso_teclado = True if int(uso_teclado) == 1 else False
	except ValueError:
		logging.info('erro, formato invalido recebido')
		raise PedidoDeSaida
	if not touchpad:
		print("mouse1")
		x = roll*MOVEMENT_SPEED
		y = pitch*MOVEMENT_SPEED
		pyautogui.moveRel(x,y)
		print("x:{} y:{}\n".format(x,y));
	elif not uso_teclado:
		print("mouse2")
		x = -pitch * MOVEMENT_SPEED_TOUCH
		y = -roll * MOVEMENT_SPEED_TOUCH
		pyautogui.moveRel(x,y,0.2)
		print("x:{} y:{}\n".format(x,y));
	if left_click:
		pyautogui.click()
	if right_click:
		pyautogui.click(button='right')
	if uso_teclado:
		pyautogui.typewrite(tecla)

def main():
	udp_ip = ""
	udp_porta = 4000
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	try:
		print("comecando servidor")
		s.bind((udp_ip, udp_porta))
		logging.info('pronto para receber conexoes')
		#s.listen(MAX_CONEXOES)
		while True:
			#conexao, endereco = s.accept()
			#print("dispositivo {} conectado".format(endereco))
			while True:
				#dados = conexao.recv(BUFFER_SIZE)
				dados,addr =  s.recvfrom(1024);
				if not dados:
					logging.info('erro na recepcao')
					conexao.close()
					break
				try:
					comando = dados.decode()
					controlaGUI(comando)
				except ValueError:
					logging.info('valor incorreto recebido')
				except PedidoDeSaida:
					logging.info('fim da conexao')
					#conexao.close()
					break
	except KeyboardInterrupt:
		try:
			#conexao.close()
			print("desconectando...")
		except:
			print("conexao ja encerrada")
		finally:
			s.close()
		
			

if __name__=="__main__":
	logging.basicConfig(filename="mouse_teclado.log",format='%(levelname)s - %(asctime)s : %(message)s', level=logging.INFO,filemode="w")
	main()
