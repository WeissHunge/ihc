package ihc.androidmouse;

import android.content.Context;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.Semaphore;

class ServerCommunication implements Runnable {

    private Context context;
    private InetAddress ip;
    private int porta;
    private final int buffer_size = 100;
    private DatagramSocket DatagramSocket;

    private Semaphore connection;

    public ServerCommunication(Context context, String ip, int porta) {
        this.context = context;
        this.porta = porta;
        connection = new Semaphore(0);
        try {
            this.ip = InetAddress.getByName(ip);
            //Log.i("ip", "ip encontrado:" + this.ip);
        } catch (UnknownHostException e) {
            //Log.i("tradução", "não foi possível traduzir endereço");
            e.printStackTrace();
        }
    }

    public void disconnect() {
        if (DatagramSocket != null)
            DatagramSocket.close();
        //Log.i("fim conexão", "DatagramSocket encerrada");
    }

    public void write(String data) {

        byte[] buffer;

        try {
            buffer = data.getBytes("UTF-8");
            while (buffer.length < buffer_size) {
                data += "$";
                buffer = data.getBytes("UTF-8");
            }
            try {
                connection.acquire();
                Thread t = new Thread(new writerThread(DatagramSocket, buffer));
                t.start();
                connection.release();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        try {
            //Log.i("conexão DatagramSocket", "começo " + ip + " " + porta);
            DatagramSocket = new DatagramSocket();
            if (DatagramSocket != null)
                connection.release();
            DatagramSocket.connect(ip, porta);
            //Log.i("conexão DatagramSocket", "criada");
        } catch (IOException e) {
            //Log.i("conexão DatagramSocket", "não foi possível fazer a conexão");
            e.printStackTrace();
        }
    }

    private class writerThread implements Runnable {

        private DatagramSocket ds;
        private byte[] data;

        writerThread(DatagramSocket ds, byte[] data) {
            this.ds = ds;
            this.data = data;
        }

        @Override
        public void run() {
            try {
                while (ds == null) ;
                DatagramPacket dp = new DatagramPacket(data, data.length);
                ds.send(dp);
                //Log.i("writing", "data: " + Arrays.toString(data));
            } catch (IOException e) {
                //Log.i("error writing", "não foi possível escrever no DatagramSocket");
                e.printStackTrace();
            }
        }
    }
}


