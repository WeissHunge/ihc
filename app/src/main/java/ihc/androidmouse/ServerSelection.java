package ihc.androidmouse;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import java.util.ArrayList;

public class ServerSelection extends AppCompatActivity {

    private ArrayAdapter<String> mArrayAdapter;
    private ArrayList<String> mArrayList;
    DataKeeper database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server_selection);
        database = new DataKeeper(getApplicationContext());
    }

    @Override
    protected void onStart() {
        super.onStart();
        mArrayList = new ArrayList<>();
        mArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mArrayList);
        showDevices();

        SQLiteDatabase rdb = database.getReadableDatabase();

        Cursor c = rdb.rawQuery("SELECT * FROM " + FeedReaderContract.TABLE_NAME, null);
        if (c.getCount() != 0) {
            c.moveToFirst();

            do {
                String row_values = "";

                for (int i = 1; i < c.getColumnCount(); i++) {
                    row_values = row_values + " " + c.getString(i);
                }

                Log.d("valores", "lido:" + row_values);
                mArrayList.add(row_values);
            } while (c.moveToNext());
        }
    }

    private boolean deleteTitle(String name) {
        SQLiteDatabase rdb = database.getReadableDatabase();
        return rdb.delete(FeedReaderContract.TABLE_NAME, FeedReaderContract.COLUMN_NAME_TITLE + "=" + name, null) > 0;
    }

    private void showDevices() {

        ListView listView = findViewById(R.id.bluetooth_devices);
        listView.setAdapter(mArrayAdapter);
        listView.setVisibility(View.VISIBLE);
        ItemClickListener cl = new ItemClickListener();
        listView.setOnItemClickListener(cl);
        listView.setOnItemLongClickListener(cl);
    }

    public void addDevice(View view) {

        AlertDialog.Builder message = new AlertDialog.Builder(ServerSelection.this);
        message.setTitle("Adição de dispositivo");
        message.setMessage("ip do dispositivo");
        final EditText input_name = new EditText(this);
        final EditText input = new EditText(this);
        input_name.setHint("nome do dispositivo");
        input.setHint("ip do dispositivo");
        final LinearLayout show_edit = new LinearLayout(this);
        show_edit.setOrientation(LinearLayout.VERTICAL);
        show_edit.addView(input);
        show_edit.addView(input_name);
        message.setView(show_edit);
        message.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mArrayList.add(input.getText().toString() + " " + input_name.getText().toString());
                Log.i("começar", "adicionado dispositivo do usuário:" + input.getText().toString());

                SQLiteDatabase wdb = database.getWritableDatabase();
                ContentValues values = new ContentValues();
                values.put(FeedReaderContract.COLUMN_NAME_TITLE, input.getText().toString());
                values.put(FeedReaderContract.COLUMN_NAME_SUBTITLE, input_name.getText().toString());
                long newRowId = wdb.insert(FeedReaderContract.TABLE_NAME, null, values);
            }
        });
        message.show();
    }

    private class ItemClickListener implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {


        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Log.i("começar", "item selecionado - " + i + " " + adapterView.getItemAtPosition(i));
            final int choosed = i;

            AlertDialog.Builder alert = new AlertDialog.Builder(ServerSelection.this);
            alert.setTitle("Click");
            alert.setMessage(Html.fromHtml("<font color=blue>você selecionou:</font> " + adapterView.getItemAtPosition(i).toString()));
            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent sensor = new Intent(getApplicationContext(), MovementCatch.class);
                    String ip = mArrayList.get(choosed).trim().split(" ")[0];
                    Log.i("ip enviado", "ipv4 - " + ip);
                    sensor.putExtra("device", ip);
                    startActivity(sensor);
                }
            });
            alert.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    return;
                }
            });

            AlertDialog dialog = alert.create();
            dialog.show();
        }

        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

            Log.v("long clicked", "pos: " + i);

            deleteTitle(String.format("'%s'", mArrayList.get(i).trim().split(" ")[0]));
            mArrayList.remove(i);
            mArrayAdapter.notifyDataSetChanged();
            return true;
        }
    }


}
