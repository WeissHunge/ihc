package ihc.androidmouse;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.ColorStateList;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

public class MovementCatch extends AppCompatActivity implements SensorEventListener {

    private ServerCommunication server_communication;
    private SensorManager mSensorManager;

    private Toast show;

    private float[] mGravity;
    private float[] mGeomagnetic;
    private float azimut;
    private float pitch;
    private float roll;

    private boolean onlyMouse = false;
    private float[] meanValue;
    private int captured = 0;
    private int MAX_ACCUMULATION = 110;
    private float slowStart = 1;
    private long lastPressTime = 0;
    private boolean mouseModeChanged = false;

    private float oldTouchX = 0;
    private float oldTouchY = 0;
    private int ac = 0;
    private boolean touchmode = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_movement_catch);
        View layout = findViewById(R.id.layout_movement_catch);
        final View botoes = findViewById(R.id.botoes);


        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view.getId() == R.id.dcButton ||
                        view.getId() == R.id.lockButton ||
                        view.getId() == R.id.LB ||
                        view.getId() == R.id.RB) {
                    return false;
                }

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // The user just touched the screen
                        if (mSensorManager != null)
                            mSensorManager.unregisterListener(getOuterClass());
                        //Log.i("toque c", "desativo sensores");

                        break;
                    case MotionEvent.ACTION_UP:
                        // The touch just ended
                        long pressTime = System.currentTimeMillis();
                        startSensor();
                        //Log.i("toque s", "ativo sensores");
                        slowStart = 0.1f;


                        if (pressTime - lastPressTime <= 300) {
                            View mouse = findViewById(R.id.LB);
                            mouse.performClick();
                        }
                        lastPressTime = pressTime;

                        break;
                }


                return true;
            }
        });

        show = Toast.makeText(this, "iniciando", Toast.LENGTH_SHORT);
        show.show();
        meanValue = new float[]{0, 0};
        TextView text_view = findViewById(R.id.coordenadas_var);
        text_view.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int orientation = getResources().getConfiguration().orientation;
                if (orientation != Configuration.ORIENTATION_LANDSCAPE || onlyMouse)
                    return;

                if (charSequence.length() <= i + i1)
                    return;

                char novo = charSequence.charAt(i + i1);
                //Log.i("char", "char novo:" + novo);
                String message = String.format(getString(R.string.defaultMessage), 0.0, 0.0, 0.0, 0, 0, 1, novo, 0);
                server_communication.write(message);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private MovementCatch getOuterClass() {
        return this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        startSensor();
        Bundle extras = getIntent().getExtras();
        String ip = extras.getString("device");
        server_communication = new ServerCommunication(this, ip, 4000);
        Thread conect = new Thread(server_communication);
        conect.start();
    }

    private void startSensor() {
        int orientation = getResources().getConfiguration().orientation;
        show.cancel();
        if (touchmode)
            return;
        if (orientation != Configuration.ORIENTATION_LANDSCAPE) {
            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null &&
                    mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null) {
                //podemos capturar os movimentos
                Sensor accelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                Sensor magneticSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
                mSensorManager.registerListener(this, accelerometerSensor, SensorManager.SENSOR_DELAY_NORMAL);
                mSensorManager.registerListener(this, magneticSensor, SensorManager.SENSOR_DELAY_NORMAL);
            } else {
                this.finishAffinity();
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;

        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;

        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];

            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                azimut = (float) Math.toDegrees(orientation[0]); // orientation contains: azimut, pitch and roll
                pitch = (float) Math.toDegrees(orientation[1]);
                roll = (float) Math.toDegrees(orientation[2]);
            }
        } else
            return;

        TextView text_view = findViewById(R.id.coordenadas_var);
        String text = String.format("<font color=red>%.2f</font> <font color=green>%.2f</font> <font color=blue>%.2f</font>", azimut, pitch, roll);

        if (captured < MAX_ACCUMULATION) {
            captured++;
            meanValue[0] += pitch;
            meanValue[1] += roll;
        }

        meanValue[0] *= slowStart;
        meanValue[1] *= slowStart;
        String message = String.format(getString(R.string.defaultMessage), azimut, meanValue[0] / MAX_ACCUMULATION, meanValue[1] / MAX_ACCUMULATION, 0, 0, 0, 'z', 0);

        slowStart = (1.3f * slowStart > 1) ? 1 : 1.3f * slowStart;
        meanValue[0] = 0;
        meanValue[1] = 0;
        captured = 0;

        server_communication.write(message);

        text_view.setText(Html.fromHtml(text));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (server_communication != null)
            server_communication.disconnect();
        if (show != null)
            show.cancel();
        if (mSensorManager != null)
            mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        return;
    }

    public void sendClick(View view) {
        view.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.buttonanim));
        int id = view.getId();
        if (id == R.id.LB) {
            String message = String.format(getString(R.string.defaultMessage), 0.0, 0.0, 0.0, 1, 0, 0, 'z', 0);
            server_communication.write(message);
        }
        if (id == R.id.RB) {
            String message = String.format(getString(R.string.defaultMessage), 0.0, 0.0, 0.0, 0, 1, 0, 'z', 0);
            server_communication.write(message);
        }
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (onlyMouse)
            return;

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            show.cancel();
            mSensorManager.unregisterListener(this);
            show.setText("modo teclado");
            show.show();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            TextView text_view = findViewById(R.id.coordenadas_var);
            text_view.setText("");


        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

            show.setText("modo mouse");
            show.show();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            assert imm != null;
            imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void endReading(View view) {
        String message = String.format(getString(R.string.defaultMessage), 0.0, 0.0, 0.0, 0, 0, 0, 'z', 1);
        server_communication.write(message);
        server_communication.disconnect();
        show.cancel();
        mSensorManager.unregisterListener(this);
        Intent intent = new Intent(this, ServerSelection.class);
        startActivity(intent);
    }

    public void lockMouseMode(View view) {
        onlyMouse = !onlyMouse;
        if (onlyMouse) {
            view.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
        } else {
            view.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
        }
    }


    public void changeMouseMode(View view) {
        mouseModeChanged = !mouseModeChanged;
        if (mouseModeChanged) {
            View v = findViewById(R.id.board);
            v.setVisibility(View.VISIBLE);
            if (mSensorManager != null)
                mSensorManager.unregisterListener(this);
            touchmode = true;
            v.setOnTouchListener(new View.OnTouchListener() {
                float accdeltaY = 0, accdeltaX = 0;

                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {

                    float oldDeltaY = accdeltaY;
                    float oldDeltaX = accdeltaX;


                    float deltaY = oldTouchY - motionEvent.getY();
                    float deltaX = oldTouchX - motionEvent.getX();

                    if (Math.abs(deltaY) > Math.abs(1 + oldDeltaY) * 10 ||
                            Math.abs(deltaX) > Math.abs(1 + oldDeltaX) * 10) {

                        oldTouchY = motionEvent.getY();
                        oldTouchX = motionEvent.getX();
                        return false;
                    }


                    oldTouchY = motionEvent.getY();
                    oldTouchX = motionEvent.getX();

                    if (ac++ < 20) {
                        accdeltaY += deltaY;
                        accdeltaX += deltaX;
                        return false;
                    }
                    ac = 0;


                    String message = String.format(getString(R.string.defaultMessage), 0.0, accdeltaX / 20, accdeltaY / 20, 0, 0, 2, 'z', 0);
                    server_communication.write(message);
                    accdeltaY = 0;
                    accdeltaX = 0;

                    //Log.d("var-touch","x:"+accdeltaX+" y:"+accdeltaY);
                    return false;
                }
            });

            view.setBackgroundTintList(ColorStateList.valueOf(Color.BLUE));
        } else {
            startSensor();
            View v = findViewById(R.id.board);
            v.setVisibility(View.INVISIBLE);
            v.removeOnAttachStateChangeListener(null);
            view.setBackgroundTintList(ColorStateList.valueOf(Color.WHITE));
        }
    }
}
